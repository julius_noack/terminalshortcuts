shortcuts
---------

### installing
#### bash

    curl https://bitbucket.org/sd_julius/terminalshortcuts/raw/20fc85b3e3d7188a2d7b7da587227410b8d539f4/shortcuts >> ~/.bashrc && source ~/.bashrc
     
#### zsh

    curl https://bitbucket.org/sd_julius/terminalshortcuts/raw/20fc85b3e3d7188a2d7b7da587227410b8d539f4/shortcuts >> ~/.zshrc && source ~/.zshrc 